class Time:
    def __init__(self, hour=0, minute=0, second=0):
        self.hour = hour
        self.minute = minute
        self.second = second

    def __str__(self):
        return f"{self.hour}:{self.minute}:{self.second}"

    def addtime(self, t1, t2):
        self.second = t1.second + t2.second
        if self.second > 60:
            self.minute = int(self.second / 60)
            self.minute = t1.minute + t2.minute + self.minute
            self.second = self.second % 60

        else:
            self.minute = t1.minute + t2.minute
        if self.minute > 60:
            self.hour = int(self.minute / 60)
            self.hour = t1.hour + t2.hour + self.hour
            self.minute = self.minute % 60
        else:
            self.hour = t1.hour + t2.hour


start = Time(1, 40, 60)
duration = Time(1, 40, 2)
result = Time()
result.addtime(start, duration)

print(result)
