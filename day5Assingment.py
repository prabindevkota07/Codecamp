class vehicle:
    def __init__(self, name, model, color):
        self.name = name
        self.model = model
        self.color = color

    def __str__(self):
        return f"{self.name} {self.color} {self.model} "


class bus(vehicle):
    def __init__(self, name, model, color, type, passenger=0):
        vehicle.__init__(self, name, model, color)
        self.type = type
        self.passenger = passenger

    def Addpassenger(self, no_of_passenger):
        self.passenger += no_of_passenger
        return f"{no_of_passenger}"

    def __str__(self):
        return vehicle.__str__(self) + f"{self.type} {self.passenger}  "


obj1 = bus('sajha', 'e444', 'red', 'delux', 13)
obj1.Addpassenger(13)
print(obj1)
