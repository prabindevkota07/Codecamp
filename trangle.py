# programme to find out weather the given sticks can form a Trangle or not
# first lets take 3 inputs from the user
stick_length = []
stick1 = int(input("Enter length of first stick :"))
stick_length.append(stick1)
stick2 = int(input("Enter length of second stick :"))
stick_length.append(stick2)
stick3 = int(input("Enter length of third stick :"))
stick_length.append(stick3)


def calculations(lengths):
    if lengths[0] + lengths[1] == lengths[2] or lengths[1] + lengths[2] == lengths[0] or lengths[0] + lengths[2] == lengths[1]:
        print("Degenerate trangle")
    elif lengths[0] + lengths[1] < lengths[2] or lengths[1] + lengths[2] < lengths[0] or lengths[0] + lengths[2] < lengths[1]:
        print("Trangle cannot be made from the given sticks")
    else:
        print("Trangle can be made")


calculations(stick_length)
print("Given lengths are " + str(stick_length))