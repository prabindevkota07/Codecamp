def histogram(word):
    output = {}
    for i in word:
        key = output.keys()
        if i not in key:
            output[i] = 1
        else:
            output[i] += 1
    return output


list_of_fruits = ['apple', 'orange', 'grapes']

for element in list_of_fruits:
    print(histogram(element))
